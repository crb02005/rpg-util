import re
import codecs

from os import listdir
from os.path import isfile, join

def parseSpell(lines):

    spell_name = str.strip(lines[0])

    def getLine(lines, line_finder):
        linesFound= [_ for _ in lines if line_finder in _]
        if(len(linesFound)>0):
            return linesFound[0]
        return ""

    def getFromLine(line, exp):
        line_search = re.search(exp,line)
        if line_search:
            return line_search.group(1)
        return ""

    def getFromLines(lines, line_finder, exp):
        return getFromLine(getLine(lines,line_finder), exp)

    school_line = getLine(lines, "School")
    subschool = getFromLine(school_line, "(\(.*\))")[1:-1]
    school = getFromLine(school_line,"^School ([^[\(\[]*)")[:-1]
    descriptor = getFromLine(school_line,"(\[.*\])")[1:-1]
    level = getFromLine(school_line,"sorcerer/wizard ([0-9]*)")

    components = getFromLines(lines, "Components", "Components (.*)$")
    if(components ==""):
        components = getFromLines(lines, "Component", "Component (.*)$")

    casting_time = getFromLines(lines, "Casting Time", "Casting Time(.*)$")
    duration =getFromLines(lines, "Duration","Duration (.*)$")
    saving_throw_line = getLine(lines, "Saving Throw")
    saving_throw = getFromLine(saving_throw_line,"Saving Throw (.*);")

    spell_resistance=""
    if(saving_throw_line!=""):
        spell_resistance =getFromLine(saving_throw_line.split(";")[1],"Spell Resistance (.*)$")
    
    effect=getFromLines(lines,"Effect","Effect (.*)$")

    description_lines = [_ for _ in range(1,len(lines)) if "DESCRIPTION" in lines[_]]
    description_index = len(lines)
    if(len(description_lines)>0):
        description_index = description_lines[0]+2
    else:
        saving_throw_lines = [_ for _ in range(1,len(lines)) if "Saving Throw " in lines[_]]
        if(len(saving_throw_lines)>0):
            description_index = saving_throw_lines[0]+2

    description = " ".join(lines[description_index:])

    return {
        "level": level,
        "spellName":spell_name,
        "source":"-",
        "shortDescription":spell_name,
        "school":school,
        "subschool":subschool,
        "descriptor":descriptor,
        "effect":effect,
        "components": components,
        "castingTime": casting_time,
        "savingThrow": saving_throw,
        "spellResistance": spell_resistance,
        "duration":duration,
        "description": description
    }

files = [f for f in listdir("spells/") if isfile(join("spells/", f))]

for filePath in files:
    print(filePath)
    lines = open("spells/"+filePath,"r", encoding="utf-8").readlines()
    print(parseSpell(lines))
