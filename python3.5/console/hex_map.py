#!/usr/bin/python3.6
import math

hex_side = 300.
border = hex_side /10
if border < 3:
    border = 3  
hex_rows = 24
hex_columns = 34
font_size = hex_side/3

half_hex_side = hex_side*.5
hex_side_squared = (hex_side*hex_side)
hyp_size = hex_side*1.1025
hyp_size_squared = (hyp_size*hyp_size)

hex_height = math.sqrt(hex_side_squared + hyp_size_squared)
half_hex_height = hex_height*.5
top_off_set = hex_side /2

font_offset_x = -half_hex_side/2
font_offset_y = (font_size/2)+half_hex_height
items = []
texts = []

hex_row_offset = 0
hex_column_offset = 0
for hex_row in range(0,hex_rows+1):
    for hex_column in range(0, hex_columns+1):
        if hex_column % 2:
            hex_row_offset = hex_row * hex_height+half_hex_height
        else:
            hex_row_offset = hex_row * hex_height
        hex_column_offset = hex_column *(hex_side+half_hex_side)
        text = {}

        x1 = hex_column_offset+half_hex_side
        x2 = hex_column_offset +hex_side+half_hex_side
        y1 = hex_row_offset
        y2 = hex_row_offset

        text["text"] = f"{hex_column} - {hex_row}"
        text["x"] = x1+font_offset_x
        text["y"] = y1+font_offset_y
        texts.append(text)
        items.append((x1,x2,y1,y2))

        x1 = hex_column_offset
        x2 = hex_column_offset+half_hex_side
        y1 = hex_row_offset+half_hex_height
        y2 = hex_row_offset
        items.append((x1,x2,y1,y2))

        x1 = hex_column_offset
        x2 = hex_column_offset+half_hex_side
        y1 = hex_row_offset+half_hex_height
        y2 = hex_row_offset+hex_height
        items.append((x1,x2,y1,y2))

        x1 = hex_column_offset+half_hex_side+hex_side
        x2 = hex_column_offset+hex_side+hex_side
        y1 = hex_row_offset
        y2 = hex_row_offset+half_hex_height
        items.append((x1,x2,y1,y2))

        x1 = hex_column_offset+half_hex_side+hex_side
        x2 = hex_column_offset+hex_side+hex_side
        y1 = hex_row_offset+hex_height
        y2 = hex_row_offset+half_hex_height
        items.append((x1,x2,y1,y2))

        x1 = hex_column_offset+half_hex_side
        x2 = hex_column_offset +hex_side+half_hex_side
        y1 = hex_row_offset+hex_height
        y2 = hex_row_offset+hex_height
        items.append((x1,x2,y1,y2))

total_width = 0
total_height = 0

for item in items:
    if total_width < item[0]:
        total_width = item[0]
    if total_width < item[1]:
        total_width = item[1]
    if total_height < item[2]:
        total_height = item[2]
    if total_height < item[3]:
        total_height = item[3]

print(f'<svg height="{total_height}" width="{total_width}">')
items = set(items)
for item in items:
    print(f'<line x1="{item[0]}" y1="{item[2]}" x2="{item[1]}" y2="{item[3]}" style="stroke:rgb(0,0,0);stroke-width:{border}"/>')
for text in texts:
    print(f' <text x="{text["x"]}" y="{text["y"]}" font-family="Courier" font-weight="bold" font-size="{font_size}">{text["text"]}</text>')
print("</svg>")
