#!/usr/bin/python3.5
from random import randint

NEW_CREATION_MULTIPLIER = .333333
REPAIR_CREATION_MULTIPLIER = .2 

def drawDashes():
	print("-"*120)

def yesOrNo(message):
    if(input("%s(y/n)\n" % (message)) == "y"):
        return True
    else:
        return False

def tryGetInt(message):
    try:
        return int(input(message))
    except:
        return tryGetInt(message)

craft_skill = tryGetInt("Craft skill?\n")
price_in_silver = tryGetInt("Price in silver?\n")
item_dc = tryGetInt("Item DC?\n")
total_raw_materials = 0

if(yesOrNo("Repair?\n")):
    cost_multiplier = REPAIR_CREATION_MULTIPLIER
else:
    cost_multiplier = NEW_CREATION_MULTIPLIER

raw_material_cost = price_in_silver * cost_multiplier
total_raw_materials += raw_material_cost
print("Initial cost for raw materials %d silver." % raw_material_cost)

if(yesOrNo("Use check per day?")):
    interval = 7
    interval_description = "day"
else:
    interval = 1
    interval_description = "week"

total_interval = 0
total_waste = 0

while(raw_material_cost > 0 and yesOrNo("Would you like to make a craft check (%d silver worth of progress remaining)?" % raw_material_cost)):
    print("Making craft check....\n")
    roll = 0
    if(yesOrNo("Do you have tools?\n")):
        if(yesOrNo("Are they masterwork artisan's?(y/n)\n")):
            roll += 2
    else:
        roll -= 2
    roll = craft_skill+tryGetInt("Roll result?\n")
    if(roll > item_dc):
        print("Craft check succeeded...\n")
        progress = (roll*item_dc)/interval
        total_interval += (raw_material_cost/(progress))
        raw_material_cost = raw_material_cost - progress
    else:
        print("Craft check failed...\n")
        if(roll < item_dc-5):
            waste = raw_material_cost*.5
            raw_material_cost += waste
            total_waste += waste
            print("You wasted %d worth of materials and had to purchase more raw materials.\n" % (waste))
            total_raw_materials += waste

drawDashes()
if(raw_material_cost < 0):
    print("You finished the item in %d %ss.\n" % (total_interval, interval_description))
else:
    print("You have a partially finished item which took you %d %ss.\n" % (total_interval, interval_description))
    print("It will take another %d worth of progress to finish\n" % (raw_material_cost))
print(" You wasted a total of %d worth of raw materials.\n You paid %d in materials. \n" % ( total_waste, total_raw_materials))
drawDashes()
